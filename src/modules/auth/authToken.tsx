let inMemoryToken = null;

export class AuthToken {
  static get() {
    return (
      inMemoryToken || localStorage.getItem('jwt') || null
    );
  };
  
  
  static clear() {
      localStorage.clear();
  };
  
  static set(token, env?) {
    localStorage.setItem('jwt', token || '');
  };

  static applyFromLocationUrlIfExists() {
    const urlParams = new URLSearchParams(
      window.location.search,
    );
    const authToken = urlParams.get('authToken');

    if (!authToken) return;

    this.set(authToken);
    window.history.replaceState(
      {},
      document.title,
      window.location.origin,
    );
  };
}
