import authAxios from 'src/modules/shared/axios/authAxios';
import { AuthToken } from 'src/modules/auth/authToken';

export default class AuthService {

  static async signinWithEmailAndPassword(username, password) {
    const urlencoded = new URLSearchParams()
    
    urlencoded.append("grant_type", "password");
    urlencoded.append("username", username);
    urlencoded.append("password", password)
    
    const response = await authAxios.post('uaa/oauth/token', urlencoded); 
    
    return response.data;
  };

  static async fetchMe() {
    const response = await authAxios.get('uaa/user/me');
    
    return response.data;
  };

  static signout() {
    AuthToken.set(null);
  };

  static async loaderPosItem(data) {
    const body = {
      data,
    };
    
    const response = await authAxios.post(
      "/restaurant/pos/item/load/",
      body,
    );
    
    return response.data;
  };
  
}
