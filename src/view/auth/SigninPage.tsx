import { useForm, FormProvider } from 'react-hook-form';
import actions from 'src/modules/auth/authActions';
import selectors from 'src/modules/auth/authSelectors';
import { i18n } from 'src/i18n';
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Content from 'src/view/auth/styles/Content';
import Logo from 'src/view/auth/styles/Logo';
import Wrapper from 'src/view/auth/styles/Wrapper';
import service from 'src/modules/auth/authService';
import InputFormItem from 'src/view/shared/form/items/InputFormItem';
import { Button } from '@material-ui/core';
import yupFormSchemas from 'src/modules/shared/yup/yupFormSchemas';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers';
import { AuthToken } from 'src/modules/auth/authToken';
import { io } from 'socket.io-client';

const schema = yup.object().shape({
  username: yupFormSchemas.string(i18n('user.fields.email'), {
    required: true,
  }),
  password: yupFormSchemas.string(
    i18n('user.fields.password'),
    {
      required: true,
    },
  )
});

const dataPos = {
  "combos": [],
  "products": [],
  "modifiers": [],
  "categories": [],
  "priceLists": [],
};

function SigninPage() {
  const dispatch = useDispatch();
  const loading = useSelector(selectors.selectLoading);
  
  const [user, setUser]= useState(null);

  const externalErrorMessage = useSelector(
    selectors.selectErrorMessage,
  );
  
  useEffect(() => {
    dispatch(actions.doClearErrorMessage());
  }, [dispatch]);

  const [initialValues] = useState({
    username: '',
    password: '',
  });

  const form = useForm({
    resolver: yupResolver(schema),
    mode: 'all',
    defaultValues: initialValues
  });

  const onSubmit = async ({ username, password }) => {
    AuthToken.clear();
    
    const { access_token } = await service.signinWithEmailAndPassword(username, password);
    
    // AuthToken.set(access_token, true);
    setUser(access_token);
    
    const socket = io("https://socket.siriuslab.app/pos", {
      auth: { token: access_token },
      query: { "clientName": "POSTest" }
    });    
    
    socket.on("connect", () => {
      alert(`${socket.id} connect`);
    });
     
    socket.on("connect_error", (err) => {
      alert(`${err} error`);
    });

    socket.on("incoming_order_pos", (order) => {
      alert(JSON.stringify(order));
    });
    
    socket.on("canceled_order_pos", (order) => {
      alert(JSON.stringify(order));
    });
    
    socket.on("send_pos_items", () => {
      alert(JSON.stringify(dataPos));     
    });
  };

  return (
    <Wrapper
      style={{
        backgroundImage: `url(${'/images/signin.jpg'})`,
      }}
    >
      <Content>
        <Logo>
            <h1>{i18n('app.title')}</h1>
        </Logo>

        <FormProvider {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <InputFormItem
              name="username"
              disabled={Boolean(user)}
              label={i18n('user.fields.email')}
              externalErrorMessage={externalErrorMessage}
            />
            <InputFormItem
              name="password"
              disabled={Boolean(user)}
              label={i18n('user.fields.password')}
              autoComplete="password"
              type="password"
            />
            <Button
              style={{ marginTop: '8px' }}
              variant="contained"
              color="primary"
              type="submit"
              disabled={Boolean(user)}
              fullWidth
            >
              {i18n(Boolean(user)? 'Connect Pos':'auth.signin')}
            </Button>
          </form>
        </FormProvider>
      </Content>
    </Wrapper>
  );
}

export default SigninPage;
