import { styled } from '@material-ui/core/styles';

const Content = styled('div')(({ theme }) => ({
  width: '500px',
  height: '50%',
  minHeight: '50%',
  overflowY: 'auto',
  zIndex: 1,
  position: 'relative',
  display: 'flex',
  flexDirection: 'column',
  padding: '56px 40px',
  backgroundColor: '#fff',
  borderRadius: "15px",
  
  [theme.breakpoints.down('sm')]: {
    width: '100%',
    borderLeft: 0,
  },
}));

export default Content;
