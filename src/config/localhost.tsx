const backendUrl = `https://api.siriuslab.app/`;

/*const backendUrl = `https://tecseris-api.jedify.io/`/**
 * Frontend Url.
 */
const frontendUrl = {
  host: 'localhost:3000',
  protocol: 'http',
};

export default {
  frontendUrl,
  backendUrl,
};
